# -*- coding: utf-8 -*-

from duralex.AbstractVisitor import AbstractVisitor

import duralex.tree

class FilterVisitor(AbstractVisitor):

    def __init__(self, fn):
        self.fn = fn

        super(FilterVisitor, self).__init__()

    def visit_node(self, node):
        if 'children' in node:
            children = list(node['children'])
            for child in children:
                if not self.fn(child):
                    duralex.tree.remove_node(node, child)

        super(FilterVisitor, self).visit_node(node)
